# Generated by Django 4.0.6 on 2022-08-01 22:22

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Projects",
            new_name="Project",
        ),
    ]
